import { Component, OnInit } from '@angular/core';
import { Event } from '../event';
import { EventService } from '../event.service';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroupDirective, FormGroup, NgForm, Validators} from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-read-event',
  templateUrl: './read-event.component.html',
  styleUrls: ['./read-event.component.sass']
})
export class ReadEventComponent implements OnInit {

  event: Event;

    constructor(
      private eventService: EventService,
      private route: ActivatedRoute,
      private location: Location
    ) { }

    myForm = new FormGroup({
      id: new FormControl('', [
      ]),
      name: new FormControl('', [
      ]),
      details: new FormControl('', [
      ]),
      startDate: new FormControl('', [
      ]),
      endDate: new FormControl('', [
      ]),
      photo: new FormControl('', [
      ]),
      audio: new FormControl('', [
      ]),
      video: new FormControl('', [
      ]),
      transcript: new FormControl('', [
      ]),
    });

    ngOnInit(): void {
      this.getEvent();

    }

    getEvent(): void {
      const id = +this.route.snapshot.paramMap.get('id');
      this.eventService.getEvent(id)
        .subscribe(event => this.event = event);
    }

}
