import { Event } from './event';


export const EVENTS: Event[] = [
{ id: 1, name: 'Inner Engineering', details: 'extra details', location: 'Los Angeles, USA', startDate: 'Sat Oct 03 2020 00:00:00 GMT+0530 (India Standard Time)', endDate: 'Sun Oct 04 2020 00:00:00 GMT+0530 (India Standard Time)', photo: '', video: '', audio: '', transcript: '' },
{ id: 2, name: 'Inner Engineering', details: 'extra details', location: 'Paris, France', startDate: 'Sat Oct 03 2020 00:00:00 GMT+0530 (India Standard Time)', endDate: 'Sun Oct 04 2020 00:00:00 GMT+0530 (India Standard Time)', photo: '', video: '', audio: '', transcript: '' },
{ id: 3, name: 'Samyama', details: 'extra details', location: 'IYC, India', startDate: 'Sat Oct 03 2020 00:00:00 GMT+0530 (India Standard Time)', endDate: 'Sun Oct 04 2020 00:00:00 GMT+0530 (India Standard Time)', photo: '', video: '', audio: '', transcript: '' },
{ id: 4, name: 'In Conversation With The Mystic', details: 'extra details', location: 'Berlin, Germany', startDate: 'Sat Oct 03 2020 00:00:00 GMT+0530 (India Standard Time)', endDate: 'Sun Oct 04 2020 00:00:00 GMT+0530 (India Standard Time)', photo: '', video: '', audio: '', transcript: '' },
{ id: 5, name: 'Mahashivratri', details: 'extra details', location: 'IYC, India', startDate: 'Sat Oct 03 2020 00:00:00 GMT+0530 (India Standard Time)', endDate: 'Sun Oct 04 2020 00:00:00 GMT+0530 (India Standard Time)', photo: '', video: '', audio: '', transcript: '' },
{ id: 6, name: 'Rally For Rivers Culmination', details: 'extra details', location: 'Delhi', startDate: 'Sat Oct 03 2020 00:00:00 GMT+0530 (India Standard Time)', endDate: 'Sun Oct 04 2020 00:00:00 GMT+0530 (India Standard Time)', photo: '', video: '', audio: '', transcript: '' },
{ id: 7, name: 'Cauvery Calling Inaugeration', details: 'extra details', location: 'IYC, Coimbatore', startDate: 'Sat Oct 03 2020 00:00:00 GMT+0530 (India Standard Time)', endDate: 'Sun Oct 04 2020 00:00:00 GMT+0530 (India Standard Time)', photo: '', video: '', audio: '', transcript: '' },
{ id: 8, name: 'Residents\' Meet', details: 'extra details', location: 'IYC, Coimbatore', startDate: 'Sat Oct 03 2020 00:00:00 GMT+0530 (India Standard Time)', endDate: 'Sun Oct 04 2020 00:00:00 GMT+0530 (India Standard Time)', photo: '', video: '', audio: '', transcript: '' },
{ id: 9, name: 'Sathsang', details: 'extra details', location: 'Theerthakund, IYC, Coimbatore', startDate: 'Sat Oct 03 2020 00:00:00 GMT+0530 (India Standard Time)', endDate: 'Sun Oct 04 2020 00:00:00 GMT+0530 (India Standard Time)', photo: '', video: '', audio: '', transcript: '' }
];
