import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Event } from './event';
import { EVENTS } from './mock-events';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class EventService {

  private eventsUrl = 'api/events';

  private handleError<T>(operation = 'operation', result?: T)
{
  return (error: any): Observable<T> => {
    console.error(error);

    console.log(`${operation} failed: ${error.message}`);

    return of(result as T);
  }
}

httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}

  createEvent(event: Event): Observable<Event> {
  return this.http.post<Event>(this.eventsUrl, event, this.httpOptions).pipe(
    tap((newEvent: Event) => console.log(`added event w/ id=${newEvent}`)),
    catchError(this.handleError<Event>('addEvent'))
  )
}
  updateEvent(event: Event): Observable<any> {
  return this.http.put<Event>(this.eventsUrl, event, this.httpOptions).pipe(
    tap(_ => console.log(`updated event w/ id=${event.id}`, event)),
    catchError(this.handleError<Event>('addEvent'))
  )
}

  getEvents(): Observable<Event[]> {
    return this.http.get<Event[]>(this.eventsUrl)
          .pipe(
            tap(_ => console.log('fetched events')),
            catchError(this.handleError<Event[]>('getEvents', []))
          );
          }

          getEvent(id: number): Observable<Event> {
            const url = `${this.eventsUrl}/${id}`;
            return this.http.get<Event>(url).pipe(
              tap(_ => console.log(`fetched event id=${id}`)),
              catchError(this.handleError<Event>(`getEvent id=${id}`))
            );
          }

  deleteEvent(event: Event | number): Observable<Event> {
    const id = typeof event === 'number' ? event: event.id;
    const url = `${this.eventsUrl}/${id}`;
    return this.http.delete<Event>(url, this.httpOptions)
          .pipe(
            tap(_ => console.log(`delete event id=${id}`)),
            catchError(this.handleError<Event>('deleteEvent'))
          );
          }

  constructor(
    private http: HttpClient
  ) { }
}
