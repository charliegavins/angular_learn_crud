import { Component, OnInit } from '@angular/core';
import { Event } from '../event';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormControl, FormGroupDirective, FormGroup, NgForm, Validators} from '@angular/forms';
import { EventService } from '../event.service';
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

// Depending on whether rollup is used, moment needs to be imported differently.
// Since Moment.js doesn't have a default export, we normally need to import using the `* as`
// syntax. However, rollup creates a synthetic default module and we thus need to import it using
// the `default as` syntax.
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment} from 'moment';

const moment = _rollupMoment || _moment;

@Component({
  selector: 'app-event',
  templateUrl: './update-event.component.html',
  styleUrls: ['./update-event.component.sass']
})
export class UpdateEventComponent implements OnInit {

  event: Event;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private eventService: EventService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getEvent();
  }

  getEvent(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    console.log(id);
    this.eventService.getEvent(id)
      .subscribe(event => {
        this.event = event;
        this.myForm.patchValue({
      id: this.event.id,
      name: this.event.name,
      details: this.event.details,
      startDate: moment(this.event.startDate),
      endDate: moment(this.event.endDate),
      audio: this.event.audio,
      video: this.event.video,
      photo: this.event.photo,
      transcript: this.event.transcript,
    });
      });
  }

  myForm = new FormGroup({
    id: new FormControl('', [
    ]),
    name: new FormControl('', [
    ]),
    details: new FormControl('', [
    ]),
    startDate: new FormControl(),
    endDate: new FormControl(),
    photo: new FormControl('', [
    ]),
    audio: new FormControl('', [
    ]),
    video: new FormControl('', [
    ]),
    transcript: new FormControl('', [
    ]),
  });

  deleteEvent(event: Event): void {
    this.eventService.deleteEvent(event).subscribe();
    this.router.navigate(['/events']);
  }

  updateEvent(): void {
    this.eventService.updateEvent(this.myForm.value)
  .subscribe(event => console.log(event));
  }

    onSubmit() {
      if (this.myForm.status !== 'INVALID') {
        console.log('valid input');
      this.updateEvent();
      this.router.navigate(['/events'])
      } else {
        console.log('invalid input');
        console.log(this.myForm.value);
      }
    }
}
