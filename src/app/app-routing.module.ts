import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent} from './homepage/homepage.component';
import { CreateEventComponent} from './create-event/create-event.component';
import { UpdateEventComponent} from './update-event/update-event.component';
import { ReadEventComponent} from './read-event/read-event.component';
import { IndexComponent} from './index/index.component';
import { SearchComponent} from './search/search.component';

const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'event/create', component: CreateEventComponent },
  { path: 'event/:id', component: ReadEventComponent },
  { path: 'event/:id/edit', component: UpdateEventComponent },
  { path: 'events', component: IndexComponent },
  { path: 'search', component: SearchComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
