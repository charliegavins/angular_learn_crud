export interface Event {
  id: number;
  name: string;
  details: string;
  location: string;
  startDate: string;
  endDate: string;
  photo: string;
  video: string;
  audio: string;
  transcript: string;
}
