import { Component, OnInit } from '@angular/core';
import { Event } from '../event';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormControl, FormGroupDirective, FormGroup, NgForm, Validators} from '@angular/forms';
import { EventService } from '../event.service';

@Component({
  selector: 'app-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.sass']
})
export class CreateEventComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private eventService: EventService,
    private location: Location
  ) { }

  ngOnInit(): void {
  }

  myForm = new FormGroup({
    name: new FormControl('', [
      Validators.required
    ]),
    details: new FormControl('', [
      Validators.required
    ]),
    startDate: new FormControl('', [
      Validators.required,
    ]),
    endDate: new FormControl('', [
      Validators.required,
    ]),
    photo: new FormControl('', [
    ]),
    audio: new FormControl('', [
    ]),
    video: new FormControl('', [
    ]),
    transcript: new FormControl('', [
    ]),
  });

  createEvent(event): void {
    this.eventService.createEvent(event)
.subscribe(event => console.log(event));
  }


    onSubmit() {
      if (this.myForm.status !== 'INVALID') {
        console.log('valid input');
      this.createEvent(this.myForm.value);
      this.router.navigate(['/events'])
      } else {
        console.log('invalid input');
        console.log(this.myForm.value);
      }
      // Do somthing
      // You can get the values in the forms like this: this.myForm.value
    }

//   goBack(): void {
//   this.location.back();
// }
//


}
